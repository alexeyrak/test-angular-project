import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TableComponent} from './table/table.component';
import {CellComponent} from './cell/cell.component';
import {SharedModule} from "../shared/shared.module";
import {FormsModule} from "@angular/forms";
import {GameService} from "./game.service";
import { ChessmenComponent } from './chessmen/chessmen.component';

@NgModule({
  declarations: [TableComponent, CellComponent, ChessmenComponent],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule
  ],
  providers: [
    GameService
  ],
  exports: [
    TableComponent
  ]
})
export class ChessModule {
}
