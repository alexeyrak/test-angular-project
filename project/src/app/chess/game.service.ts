import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs/internal/BehaviorSubject";
import {Game} from "../../chess-engine/game";
import {IPosition} from "../../chess-engine/position";
import {Chessmen} from "../../chess-engine/chessmen/chessmen";
import {FIELD_SIZE} from "../../chess-engine/field-size.constants";

const ROTATED_DEFAULT: boolean = true;

let JSON_STATE: any = null;
@Injectable()
export class GameService {
  LS_KEY: string = '__save_game__';
  private _rotated: boolean = ROTATED_DEFAULT;
  private _game: Game;
  changes$: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  bgColor$: any = new BehaviorSubject('#d5d5d5');
  blackCellColor$: any = new BehaviorSubject('#7b3000');
  constructor() {
    this.newGame();
  }

  click(position: IPosition) {
    if(this._game.action(position)) {
      // По ТЗ каждый ход состояние должно сохранятся как JSON обьект
      // "Создать JSON представление доски и актуализировать его во время каждого хода"
      JSON_STATE = this.game.export();
    }
    this.changes$.next(null);
  }

  save() {
    localStorage.setItem(this.LS_KEY, JSON.stringify(Object.assign({}, this.game.export(), {
      rotated: this._rotated
    })));
  }

  newGame() {
    this._game = new Game();
    this._rotated = ROTATED_DEFAULT;
    this.changes$.next(null);
  }

  load() {
    let storeStr = localStorage.getItem(this.LS_KEY);
    let store = storeStr ? JSON.parse(storeStr) : null;

    if(store) {
      this._game = new Game(store);
      this.changes$.next(null);
      this._rotated = store.rotated;
    } else {
      this.newGame();
    }
  }


  get game(): Game {
    return this._game;
  }

  get selected() {
    return this._game.selected;
  }

  get deads() {
    return this._game.chessmenList.filter((chessmen: Chessmen) => !chessmen.position.isInGame());
  }

  getVerticalMarker(index: number) {
    return index + 1;
  }

  getHorizontalMarker(index: number) {
    return String.fromCharCode('a'.charCodeAt(0) + FIELD_SIZE - index - 1);
  }


  get isRotated(): boolean {
    return this._rotated;
  }

  rotate() {
    this._rotated = !this._rotated;
  }
}
