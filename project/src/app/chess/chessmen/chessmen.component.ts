import {Component, Input, OnInit} from '@angular/core';
import {Chessmen} from "../../../chess-engine/chessmen/chessmen";
import {GameService} from "../game.service";
import {SIDES} from "../../../chess-engine/sides.constant";

@Component({
  selector: 'app-chessmen',
  templateUrl: './chessmen.component.html',
  styleUrls: ['./chessmen.component.css']
})
export class ChessmenComponent implements OnInit {
  @Input() chessmen: Chessmen = null;
  constructor( ) { }

  ngOnInit() {
  }
}
