import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChessmenComponent } from './chessmen.component';
import {Chessmen} from "../../../chess-engine/chessmen/chessmen";
import {SIDES} from "../../../chess-engine/sides.constant";
import {CHESSMEN_TYPES} from "../../../chess-engine/chessmen/chessmen-type.constant";
import {Position} from "../../../chess-engine/position";

describe('ChessmenComponent', () => {
  let component: ChessmenComponent;
  let fixture: ComponentFixture<ChessmenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChessmenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChessmenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create chessmen CSS class', () => {
    component.chessmen = new Chessmen({
      side: SIDES.WHITE,
      type: CHESSMEN_TYPES.QUEEN,
      position: new Position(1, 2)
    });
    fixture.detectChanges();
    let chessmen = fixture.nativeElement.querySelector('.chessmen');

    expect(chessmen.classList.contains(`${SIDES.WHITE}_${CHESSMEN_TYPES.QUEEN}`)).toBeTruthy();
  });
});
