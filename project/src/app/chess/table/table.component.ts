import {Component, OnDestroy, OnInit} from '@angular/core';
import {GameService} from "../game.service";
import {Subscription} from "rxjs/internal/Subscription";
import {Chessmen} from "../../../chess-engine/chessmen/chessmen";
import {FIELD_SIZE} from "../../../chess-engine/field-size.constants";
import {SIDES} from "../../../chess-engine/sides.constant";

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit, OnDestroy {
  cells: any[][];
  private whiteDeads: Chessmen[];
  private blackDeads: Chessmen[];
  private ganeChangesSub: Subscription;
  private isWhitesMove: boolean;

  constructor(
    private gameService: GameService
  ) {
    this.cells = new Array(FIELD_SIZE)
      .fill(new Array(FIELD_SIZE).fill(0));
    this.ganeChangesSub = gameService.changes$
      .subscribe(() => {
        this.whiteDeads = this.gameService.deads.filter((chessmen: Chessmen) => chessmen.side === SIDES.WHITE);
        this.blackDeads = this.gameService.deads.filter((chessmen: Chessmen) => chessmen.side === SIDES.BLACK);
        this.isWhitesMove = this.gameService.game.isWhitesMove;
      })

  }

  ngOnInit() {
  }

  get color1() {
    return this.gameService.bgColor$;
  }

  get color2() {
    return this.gameService.blackCellColor$;
  }

  save() {
    this.gameService.save();
  }

  load() {
    this.gameService.load();
  }

  newGame() {
    this.gameService.newGame();
  }

  rotate() {
    this.gameService.rotate();
  }

  get isRotated() {
    return this.gameService.isRotated;
  }

  ngOnDestroy(): void {
    this.ganeChangesSub.unsubscribe();
  }

  getMarkerClass(x, y) {
    let classes = [];
    if(y === 0) {
      classes.push('top');
    }
    if (y === FIELD_SIZE - 1) {
      classes.push('bottom');
    }
    if(x === 0) {
      classes.push('left');
    }
    if (x === FIELD_SIZE -1) {
      classes.push('right');
    }
    if(classes.length){
      classes.push('marker-container');
    }
    return classes;
  }

  getHorizontalMarker(index: number) {
    return this.gameService.getHorizontalMarker(index);
  }

  getVerticalMarker(index: number) {
    return this.gameService.getVerticalMarker(index);
  }
}
