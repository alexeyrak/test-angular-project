import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {map} from "rxjs/operators";
import {Subscription} from "rxjs/internal/Subscription";
import {GameService} from "../game.service";
import {Chessmen} from "../../../chess-engine/chessmen/chessmen";
import {Position} from "../../../chess-engine/position";
import {SIDES} from "../../../chess-engine/sides.constant";

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css']
})
export class CellComponent implements OnInit, OnDestroy {
  @Input() x: number;
  @Input() y: number;
  chessmen: Chessmen;
  private changeSub: Subscription;
  private isSelectedMove: boolean;
  private isSelectedHit: boolean;
  private yourMove: boolean;

  constructor(
    private gameService: GameService
  ) { }

  ngOnInit() {
    this.changeSub = this.gameService.changes$.subscribe(() => this.updateData());
  }

  updateData() {
    this.chessmen = this.gameService.game.getChessmenByPosition(new Position(this.x, this.y));
    this.isSelectedMove = this.findThisPosition(this.gameService.game.selected, 'movements');
    this.isSelectedHit = this.findThisPosition(this.gameService.game.selected, 'hits');
    this.yourMove = ((this.isSelectedMove || this.isSelectedHit)
      || this.chessmen && (this.gameService.game.isWhitesMove !== (this.chessmen.side === SIDES.BLACK)));
  }

  findThisPosition(data: any, field: string)  {
    if(data && data[field]) {
      return !!data[field].find((position: Position) => position.isEqual({x: this.x, y: this.y}))
    } else {
      return false;
    }
  }

  get color() {
    return this.gameService.blackCellColor$.pipe(map((color) => this.isBlack ? color : ''));
  }

  get isBlack() {
    return this.y % 2 ^ this.x % 2;
  }

  click() {
    this.gameService.click(new Position(this.x, this.y));
    console.log(this.x, this.y);
  }

  ngOnDestroy(): void {
    this.changeSub.unsubscribe();
  }

  dragover(e: any){
    e.preventDefault();
  }
}
