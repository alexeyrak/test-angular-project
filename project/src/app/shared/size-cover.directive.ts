import {AfterViewInit, Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[appSizeCover]'
})
export class SizeCoverDirective implements AfterViewInit {

  constructor(private el: ElementRef) {
  }

  ngAfterViewInit(): void {
    this.resize();
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.resize();
  }

  resize() {
    let el: any = this.el.nativeElement;
    let parent: any = el.parentElement;
    el.style.display = 'none';
    let size: number = parent.offsetHeight > parent.offsetWidth ? parent.offsetWidth : parent.offsetHeight;
    el.style.width = `${size}px`;
    el.style.height = `${size}px`;
    el.style.display = '';
  }
}
