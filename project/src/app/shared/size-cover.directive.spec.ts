import { SizeCoverDirective } from './size-cover.directive';
import {ElementRef} from "@angular/core";

let element: any;
let parent: any;

let sizeCoverDirective: SizeCoverDirective;
describe('SizeCoverDirective', () => {

  beforeEach(() => {
    element = document.createElement('div');
    parent = document.createElement('div');
    parent.append(element);
    document.body.append(parent);
    sizeCoverDirective = new SizeCoverDirective(new ElementRef(element));
  });
  it('should create an instance', () => {
    expect(sizeCoverDirective).toBeTruthy();
  });

  it('should resize by parent width if it shorter then height', () => {
    parent.style.width = '500px';
    parent.style.height = '800px';
    sizeCoverDirective.resize();
    expect(element.offsetWidth).toBe(500);
  });

  it('should resize by parent height if it shorter then width', () => {
    parent.style.width = '1000px';
    parent.style.height = '800px';
    sizeCoverDirective.resize();
    expect(element.offsetHeight).toBe(800);
  });

  it('should be equal (height === weight)', () => {
    parent.style.width = '1000px';
    parent.style.height = '800px';
    sizeCoverDirective.resize();
    expect(element.offsetHeight).toBe(element.offsetWidth);
  });
});
