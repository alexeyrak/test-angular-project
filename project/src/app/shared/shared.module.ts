import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SizeCoverDirective } from './size-cover.directive';

@NgModule({
  declarations: [SizeCoverDirective],
  imports: [
    CommonModule
  ],
  exports: [
    SizeCoverDirective
  ]
})
export class SharedModule { }
