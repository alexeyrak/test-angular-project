import {IPosition, Position} from "../position";
import {CHESSMEN_TYPES} from "./chessmen-type.constant";
import {StraightDown, StraightLeft, StraightRight, StraightUp} from "../movement/straight";
import {DiagonallyDownLeft, DiagonallyDownRight, DiagonallyUpLeft, DiagonallyUpRight} from "../movement/diagonally";
import {BaseMovement} from "../movement/base-movement";
import {
  KnightDownLeft,
  KnightDownRight, KnightLeftDown, KnightLeftUp,
  KnightRightDown,
  KnightRightUp,
  KnightUpLeft,
  KnightUpRight
} from "../movement/knight";
import {SIDES} from "../sides.constant";
import {FIELD_SIZE} from "../field-size.constants";

export interface IChessmen {
  type: string;
  side: string;
  position?: IPosition;
}

export class Chessmen implements IChessmen {
  protected movements: BaseMovement[];
  protected hits: BaseMovement[];

  private _position: Position;
  private _type: string;
  private _side: string;

  constructor({type, side, position}: IChessmen = {type: null, side: null, position: null}) {
    this._type = type;
    this._side = side;
    this._position = Position.byPosition(position);
  }

  isEqual(el: any = {}) {
    return this.side === el.side&&this.type===el.type;
  }

  get type(): string {
    return this._type;
  }

  get side(): string {
    return this._side;
  }

  get movePositions() {
     return this.movements.map((movement: BaseMovement) => movement.getPositions(this._position));
  }

  get hitPositions() {
     return this.hits.map((movement: BaseMovement) => movement.getPositions(this._position));
  }


  get position(): Position {
    return this._position;
  }

  set position(value: Position) {
    this._position = Position.byPosition(value);
  }
}

export class King extends Chessmen {
  constructor(side: string) {
    super({type: CHESSMEN_TYPES.KING, side});
    this.hits = this.movements = [
      new StraightUp(1),
      new StraightDown(1),
      new StraightLeft(1),
      new StraightRight(1),
      new DiagonallyUpRight(1),
      new DiagonallyUpLeft(1),
      new DiagonallyDownRight(1),
      new DiagonallyDownLeft(1)
    ];
  }
}

export class Queen extends Chessmen {
  constructor(side: string) {
    super({type: CHESSMEN_TYPES.QUEEN, side});
    this.hits = this.movements = [
      new StraightUp(),
      new StraightDown(),
      new StraightLeft(),
      new StraightRight(),
      new DiagonallyUpRight(),
      new DiagonallyUpLeft(),
      new DiagonallyDownRight(),
      new DiagonallyDownLeft()
    ];
  }
}

export class Bishop extends Chessmen {
  constructor(side: string) {
    super({type: CHESSMEN_TYPES.BISHOP, side});
    this.hits = this.movements = [
      new DiagonallyUpRight(),
      new DiagonallyUpLeft(),
      new DiagonallyDownRight(),
      new DiagonallyDownLeft()
    ];
  }

}

export class Knight extends Chessmen {
  constructor(side: string) {
    super({type: CHESSMEN_TYPES.KNIGHT, side});
    this.hits = this.movements = [
      new KnightUpRight(1),
      new KnightUpLeft(1),
      new KnightDownRight(1),
      new KnightDownLeft(1),
      new KnightRightUp(1),
      new KnightRightDown(1),
      new KnightLeftUp(1),
      new KnightLeftDown(1),
    ];
  }

}

export class Rock extends Chessmen {
  constructor(side: string) {
    super({type: CHESSMEN_TYPES.ROCK, side});
    this.hits = this.movements = [
      new StraightUp(),
      new StraightDown(),
      new StraightLeft(),
      new StraightRight()
    ];
  }

}

export class Pawn extends Chessmen {
  constructor(side: string) {
    super({type: CHESSMEN_TYPES.PAWN, side});
    this.movements = this.side === SIDES.WHITE ? [
      new StraightUp(1),
    ] : [
      new StraightDown(1),
    ];
    this.hits = this.side === SIDES.WHITE ? [
      new DiagonallyUpRight(1),
      new DiagonallyUpLeft(1),
    ]: [
      new DiagonallyDownRight(1),
      new DiagonallyDownLeft(1),
    ];
  }

  get movePositions() {
    return (this.position.y === 1 || this.position.y === FIELD_SIZE - 2
      ? (this.side === SIDES.WHITE ? [new StraightUp(2)] : [new StraightDown(2)])
      : this.movements)
      .map((movement: BaseMovement) => movement.getPositions(this.position));
  }

}

export const chessmenFactory = (chessmen: IChessmen) => {
  switch (chessmen.type) {
    case CHESSMEN_TYPES.KING:
      return new King(chessmen.side);
    case CHESSMEN_TYPES.QUEEN:
      return new Queen(chessmen.side);
    case CHESSMEN_TYPES.BISHOP:
      return new Bishop(chessmen.side);
    case CHESSMEN_TYPES.KNIGHT:
      return new Knight(chessmen.side);
    case CHESSMEN_TYPES.ROCK:
      return new Rock(chessmen.side);
    case CHESSMEN_TYPES.PAWN:
      return new Pawn(chessmen.side);
  }
};
