export const CHESSMEN_TYPES: any = {
    KING: 'king',
    QUEEN: 'queen',
    KNIGHT: 'knight',
    ROCK: 'rock',
    BISHOP: 'bishop',
    PAWN: 'pawn',
};
