import {SIDES} from "./sides.constant";
import {arrayOfArrayToArray} from "../utils/index";
import {CHESSMEN_AMOUNT} from "./placement/chessmen-amount";
import {Placement} from "./placement/placement";
import {Chessmen, chessmenFactory} from "./chessmen/chessmen";
import {IPosition} from "./position";


class Side {
  constructor(private currentSide: string = SIDES.WHITE) {}

  isCurrent(side: string) {
    return this.currentSide === side;
  }
  isWhite() {
    return this.isCurrent(SIDES.WHITE);
  }
  toggle() {
    if(this.currentSide === SIDES.WHITE) {
      this.currentSide = SIDES.BLACK;
    } else {
      this.currentSide = SIDES.WHITE;
    }
  }
  toString() {
    return this.currentSide;
  }
}

export class Game {
  private placement: Placement;
  selected: any = null;

  private side: Side;
  constructor(store: any = {}) {
    this.placement = new Placement(store.placement);
    this.side = new Side(store.side);
  }

  export() {
    return {
      placement: this.placement.toJSON(),
      side: this.side.toString()
    }
  }

  get isWhitesMove() {
    return this.side.isWhite();
  }

  action(position: IPosition) {
    let selected = this.placement.getChessmenByPosition(position);
    if(selected && this.side.isCurrent(selected.side)) {
      this.select(selected);
    } else {
      if(this.selected && this.move(position)) {
        this.disselect();
        this.afterMove();
        return true;
      }
      this.disselect();
    }
    return false;
  }

  get chessmenList() {
    return this.placement.chessmenList;
  }

  private afterMove() {
    this.side.toggle()
  }

  getChessmenByPosition(position: IPosition) {
    return this.placement.getChessmenByPosition(position);
  }

  private move(position: IPosition) {
    return this.placement.move(this.selected.chessmen, position);
  }

  private disselect() {
    this.selected = null;
  }

  private select(selected: Chessmen) {
    let actions = this.placement.getActions(selected);
    this.selected = Object.assign({}, actions, {
      chessmen: selected
    });
  }
  private generateList() {
    let sideList: string[] = Object.keys(SIDES).map((key: string) => SIDES[key]);
    return arrayOfArrayToArray(sideList.map((side: string) => arrayOfArrayToArray(CHESSMEN_AMOUNT
        .map((obj: any) => new Array(obj.amount)
          .fill(1).map(() => chessmenFactory({type: obj.chessmenType, side}))))));
  }


}

