import {FIELD_SIZE} from "./field-size.constants";

export interface IPosition {
  x: number;
  y: number;
}

export class Position implements IPosition{
  static byPosition(position: IPosition = {x: null, y: null}) {
    return new Position(position.x, position.y);
  }

  constructor(
    private _x: number = null,
    private _y: number = null
  ) {}

  isEqual(el: any = {}) {
    return this.x === el.x && this.y === el.y;
  }

  isInGame() {
    return this.x >= 0 && this.x < FIELD_SIZE && this.y >= 0 && this.y < FIELD_SIZE && this.x !== null && this.y !== null;
  }

  get x(): number {
    return this._x;
  }

  get y(): number {
    return this._y;
  }

  toKey() {
    return `${this.x}_${this.y}`
  }
}
