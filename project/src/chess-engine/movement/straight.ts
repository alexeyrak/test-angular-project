import {BaseMovement} from "./base-movement";
import {IPosition, Position} from "../position";

export class StraightUp extends BaseMovement{
  next(position: IPosition): IPosition {
    return new Position(position.x, position.y + 1);
  }
}

export class StraightRight extends BaseMovement{
  next(position: IPosition): IPosition {
    return new Position(position.x + 1, position.y);
  }
}
export class StraightDown extends BaseMovement{
  next(position: IPosition): IPosition {
    return new Position(position.x, position.y - 1);
  }
}
export class StraightLeft extends BaseMovement{
  next(position: IPosition): IPosition {
    return new Position(position.x - 1, position.y);
  }
}
