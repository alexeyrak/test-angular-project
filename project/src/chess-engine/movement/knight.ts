import {BaseMovement} from "./base-movement";
import {IPosition, Position} from "../position";

export class KnightUpRight extends BaseMovement{
  next(position: IPosition): IPosition {
    return new Position(position.x + 1, position.y + 2);
  }
}
export class KnightUpLeft extends BaseMovement{
  next(position: IPosition): IPosition {
    return new Position(position.x - 1, position.y + 2);
  }
}

export class KnightRightUp extends BaseMovement{
  next(position: IPosition): IPosition {
    return new Position(position.x + 2, position.y + 1);
  }
}
export class KnightRightDown extends BaseMovement{
  next(position: IPosition): IPosition {
    return new Position(position.x + 2, position.y - 1);
  }
}
export class KnightDownRight extends BaseMovement{
  next(position: IPosition): IPosition {
    return new Position(position.x + 1, position.y - 2);
  }
}
export class KnightDownLeft extends BaseMovement{
  next(position: IPosition): IPosition {
    return new Position(position.x - 1, position.y - 2);
  }
}
export class KnightLeftUp extends BaseMovement{
  next(position: IPosition): IPosition {
    return new Position(position.x - 2, position.y + 1);
  }
}
export class KnightLeftDown extends BaseMovement{
  next(position: IPosition): IPosition {
    return new Position(position.x - 2, position.y - 1);
  }
}
