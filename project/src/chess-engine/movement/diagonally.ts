import {BaseMovement} from "./base-movement";
import {IPosition, Position} from "../position";

export class DiagonallyUpRight extends BaseMovement{
  next(position: IPosition): IPosition {
    return new Position(position.x + 1, position.y + 1);
  }
}

export class DiagonallyDownRight extends BaseMovement{
  next(position: IPosition): IPosition {
    return new Position(position.x + 1, position.y - 1);
  }
}
export class DiagonallyDownLeft extends BaseMovement{
  next(position: IPosition): IPosition {
    return new Position(position.x - 1, position.y - 1);
  }
}
export class DiagonallyUpLeft extends BaseMovement{
  next(position: IPosition): IPosition {
    return new Position(position.x - 1, position.y + 1);
  }
}
