import {FIELD_SIZE} from "../field-size.constants";
import {IPosition} from "../position";

export const isFieldCell = (position: IPosition) => {
  return position.x >= 0 && position.x < FIELD_SIZE && position.y >= 0 && position.y < FIELD_SIZE;
};

export class BaseMovement {
  constructor(private steps: number = null) {}

  getPositions(position: IPosition) {
    let currentPosition: IPosition = this.next(position);
    let positions: IPosition[] = [];
    let counter = 0;
    while (isFieldCell(currentPosition) && (!this.steps || this.steps > counter)) {
      positions.push(currentPosition);
      currentPosition = this.next(currentPosition);
      ++counter;
    }
    return positions;

  }

  next(position: IPosition): IPosition {
    return null;
  };
}
