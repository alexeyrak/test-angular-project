import {StraightUp} from "./straight";
import {Position} from "../position";
import {FIELD_SIZE} from "../field-size.constants";

let straightUp: StraightUp;
describe('StraightUp (movement)', () => {

  beforeEach(() => {
    straightUp = new StraightUp();
  });

  it('should create', () => {
    expect(straightUp).toBeTruthy();
  });

  it('should don\'t move left or right', () => {
    let startPos = new Position(0, 0);
    let newPos = straightUp.next(startPos);
    expect(startPos.x).toBe(newPos.x);
  });

  it('should move up', () => {
    let startPos = new Position(0, 0);
    let newPos = straightUp.next(startPos);
    expect(newPos.y - 1).toBe(startPos.y);
  });

  it('should create array by getPositions', () => {
    let chessmenPosList = straightUp.getPositions(new Position(0, 0));
    expect(Array.isArray(chessmenPosList)).toBeTruthy();
  });

  it('should create points until edge', () => {
    let chessmenPosList = straightUp.getPositions(new Position(0, 0));
    expect(chessmenPosList.length).toBe(7);
  });

  it('should create points in the field', () => {
    let chessmenPosList = straightUp.getPositions(new Position(0, 0));
    expect(chessmenPosList.every((pos: Position) => pos.y >= 0 && pos.y < FIELD_SIZE))
      .toBeTruthy();
  });

  it('should return empty array if start point on the edge', () => {
    let chessmenPosList = straightUp.getPositions(new Position(0, 7));
    expect(chessmenPosList.length).toBe(0);
  });

  it('should create points amount equal to steps', () => {
    let straightUp: StraightUp = new StraightUp(2);
    let chessmenPosList = straightUp.getPositions(new Position(0, 0));
    expect(chessmenPosList.length).toBe(2);
  });
});
