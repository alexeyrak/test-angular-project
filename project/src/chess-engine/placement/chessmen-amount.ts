import {CHESSMEN_TYPES} from "../chessmen/chessmen-type.constant";

export const CHESSMEN_AMOUNT: any[] = [
  {
    chessmenType: CHESSMEN_TYPES.KING,
    amount: 1
  },
  {
    chessmenType: CHESSMEN_TYPES.QUEEN,
    amount: 1
  },
  {
    chessmenType: CHESSMEN_TYPES.BISHOP,
    amount: 2
  },
  {
    chessmenType: CHESSMEN_TYPES.KNIGHT,
    amount: 2
  },
  {
    chessmenType: CHESSMEN_TYPES.ROCK,
    amount: 2
  },
  {
    chessmenType: CHESSMEN_TYPES.PAWN,
    amount: 8
  }
];
