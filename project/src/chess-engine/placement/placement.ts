import {START_CHESSMEN_MAP} from "./start-chessmen-map.constant";
import {FIELD_SIZE} from "../field-size.constants";
import {BehaviorSubject} from "rxjs/internal/BehaviorSubject";
import {IPosition, Position} from "../position";
import {Chessmen, chessmenFactory, IChessmen} from "../chessmen/chessmen";
import {SIDES} from "../sides.constant";
import {arrayOfArrayToArray} from "../../utils/index";
import {CHESSMEN_AMOUNT} from "./chessmen-amount";


class Cache<T> {
  private data: {['string']?: T} = {};
  setItem(key: string, value: T) {
    this.data[key] = value;
  }
  getItem(key: string): T {
    return this.data[key];
  }

  clear() {
    this.data = {};
  }
}

export interface IConf {
  position: IPosition,
  chessmen: IChessmen
};

export interface IAction {
  movements: IPosition[];
  hits: IPosition[];
}


export class Placement {
  private _chessmenList: Chessmen[];
  private actionCache: Cache<IAction> = new Cache<IAction>();
  private chessmenCache: Cache<IAction> = new Cache<IAction>();

  constructor(inPositions?: IConf[]) {
    let positions = inPositions || this.byStartMap();
    let chessmenList: Chessmen[] = this.generateList();
    positions.forEach((conf: IConf) => {
      let freeChessmen: Chessmen = chessmenList
        .find((chessmen =>  (!chessmen.position.isInGame()) && chessmen.isEqual(conf.chessmen)));
      freeChessmen.position = Position.byPosition(conf.position);
    });
    this._chessmenList = chessmenList;
  }

  toJSON() {
    return this._chessmenList
      .filter((checsmen: Chessmen) => checsmen.position.isInGame())
      .map((checsmen: Chessmen) => ({
        chessmen: {
          type: checsmen.type,
          side: checsmen.side
        },
        position: {
          x: checsmen.position.x,
          y: checsmen.position.y
        }
      }));
  }

  get chessmenList(): Chessmen[] {
    return this._chessmenList;
  }

  getChessmenByPosition(position: IPosition): Chessmen {
    return this._chessmenList
        .find((chessmen: Chessmen) => chessmen.position.isEqual(new Position(position.x, position.y)));
  }

  move(chessmen: Chessmen, newPosition: IPosition) {
    let newPos = Position.byPosition(newPosition);
    let actions = this.getActions(chessmen);
    let destChessmen = this.getChessmenByPosition(newPosition);
    if(destChessmen) {
      let canMove = !!actions.hits.find((position: Position) => position.isEqual(newPosition));
      if(canMove) {
        chessmen.position = newPos;
        destChessmen.position = new Position();
        this.moveSuccess();
        return true;
      }
    } else {
      let canMove = !!actions.movements.find((position: Position) => position.isEqual(newPosition));
      if (canMove) {
        chessmen.position = newPos;
        this.moveSuccess();
        return true;
      }
    }
    return false;
  }

  private moveSuccess() {
    this.chessmenCache.clear();
    this.actionCache.clear();
  }

  getActions(chessmen: Chessmen) {
    let key: string = chessmen.position.toKey();
    if(!this.actionCache.getItem(key)) {
      this.actionCache.setItem(key, {
        movements: arrayOfArrayToArray(this.filterMovements(chessmen.movePositions)),
        hits: arrayOfArrayToArray( this.filterHits(chessmen.hitPositions, chessmen.side)),
      });
    }
    return this.actionCache.getItem(key);
  }

  private filterMovements(positions: IPosition[][]) {
    return positions.map((positionList: IPosition[]) => {
      let chessmenPosition = positionList.find((position: IPosition) => {
        return !!this.getChessmenByPosition(position);
      });
      return !chessmenPosition ? positionList : positionList.slice(0, positionList.indexOf(chessmenPosition));
    });
  }

  private filterHits(positions: IPosition[][], side: string) {
    return positions.map((positionList: IPosition[]) =>{
      let chessmenPosition = positionList.find((position: IPosition) => !!this.getChessmenByPosition(position));
      return chessmenPosition && (this.getChessmenByPosition(chessmenPosition).side !== side) ? [chessmenPosition] :[];
    });
  }


  private generateList() {
    let sideList: string[] = Object.keys(SIDES).map((key: string) => SIDES[key]);
    return arrayOfArrayToArray(sideList.map((side: string) => arrayOfArrayToArray(CHESSMEN_AMOUNT
      .map((obj: any) => new Array(obj.amount)
        .fill(1).map(() => chessmenFactory({type: obj.chessmenType, side}))))));
  }

  byStartMap() {
    let sides = Object.keys(SIDES).map((key: string) => SIDES[key]);
    let positions = arrayOfArrayToArray(sides
      .map((side: string) => arrayOfArrayToArray(START_CHESSMEN_MAP
        .map((chessmenTypeRow: string[], rowIndex: number) => chessmenTypeRow
          .map((chessmenType: string, colIndex: number) => {
            let row = side === SIDES.WHITE ? rowIndex : FIELD_SIZE - rowIndex - 1;
            let col =  colIndex;
            return {position: new Position(col, row), chessmen: {type: chessmenType, side}};
          })))));
    return positions;
  }

}
