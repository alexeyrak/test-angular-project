export const arrayOfArrayToArray = (list: any[][]) => {
  return list.reduce((total: any[], listItem: string[]) => {
        return [...total, ...listItem];
      }, []);
};
